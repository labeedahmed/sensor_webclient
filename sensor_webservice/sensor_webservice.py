"""API with Python that allows recording and getting a list of sensor values"""
from datetime import datetime

from flask import Flask, abort, jsonify

app = Flask("temperature-webservice")

# Array for saving temperature and time data
temperature_data = {}


@app.route("/sensor/temperature/<float:value>")
def record_temp(value):
    """Assign value and timestamp together and save into a list of recorded data.

    timestamp: current time when submitting request; type: datetime
    index: current size of temperature_data array; type: int
    value: the value entered by the user; type: float
    """

    timestamp = str(datetime.now(tz=None))
    index = len(temperature_data)
    temperature_data[index] = {"time": timestamp, "value": value}
    return "Saved a new temperature value: " + str(value)


@app.route("/sensor/temperature")
def return_records():
    """Jsonify the temperature_data array and return temperature data"""

    return jsonify(temperature_data)


@app.route("/sensor/temperature/<incorrect_value>")
def record_incorrect(incorrect_value):
    """Throw exception with HTML-Status 400 - Bad Request"""
    if not isinstance(incorrect_value, float):
        abort(400, description="Please enter a value of type float.")


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)  # pragma: no cover
