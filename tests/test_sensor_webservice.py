"""Test for temperature_api.py"""
from sensor_webservice.sensor_webservice import app


def test_temperature_api_file():
    """Test-client to test the functions of the API for different inputs."""
    with app.test_client() as test_app:
        # Test if input of a float value returns status 200
        response = test_app.get('/sensor/temperature/32.0')
        assert response.status_code == 200
        assert "Saved a new temperature value: 32.0" in response.data.decode()

        # Test if input of a string value returns an error code 400
        response = test_app.get('/sensor/temperature/thisisnotafloatvalue')
        assert response.status_code == 400
        assert "Please enter a value of type float." in response.data.decode()

        # Test if input of an int value returns an error code 400
        response = test_app.get('/sensor/temperature/32')
        assert response.status_code == 400
        assert "Please enter a value of type float." in response.data.decode()

        # Test if output-route returns the right output
        response = test_app.get('/sensor/temperature')
        assert response.status_code == 200
        assert response.headers.get('content-type') == 'application/json'
